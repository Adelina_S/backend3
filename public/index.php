<?php

header('Content-Type: text/html; charset=UTF-8');

if ($_SERVER['REQUEST_METHOD'] == 'GET')
{
    if (!empty($_GET['save']))
    {
      print('Спасибо, результаты сохранены.');
    }
    include('form.php');
    exit();
}

try
{
  $errors = FALSE;

  if (empty($_POST['name']))
  {
    print('Заполните имя.<br/>');
    $errors = TRUE;
  }

  if (empty($_POST['email']))
  {
    print('Заполните почту.<br/>');
    $errors = TRUE;
  }

  if (empty($_POST['bio']))
  {
    print('Заполните биографию.<br/>');
    $errors = TRUE;
  }

  if (empty($_POST['check']))
  {
    print('Вы должны быть согласны с условиями.<br/>');
    $errors = TRUE;
  }

  if ($errors)
  {
    exit();
  }

  $name = $_POST['name'];
  $email = $_POST['email'];
  $year = $_POST['yearb'];
  $sex = $_POST['sex'];
  $limb = $_POST['limbs'];
  $spw = $_POST['superpwr'];
  $biography = $_POST['bio'];

  $db = new PDO('mysql:host=localhost;dbname=u16306', 'u16306', '2435324', array(PDO::ATTR_PERSISTENT => true));

  $stmt = $db->prepare("INSERT INTO `users` (`name`, `emale`, `date`, `gender`, `numb_limbs`, `biogr`) VALUES (:name, :email, :year, :sex, :limb, :biography)");
  $stmt -> execute(array(':name' => $name, ':email' => $email, ':year' => $year, ':sex' => $sex, ':limb' => $limb, ':biography' => $biography));

  $stmt = $db->prepare("SELECT `id` FROM `users` WHERE name = :name AND emale = :email");
  $stmt->execute(array(':name' => $name, ':email' => $email));
  $userid = $stmt->fetchAll();

  $spwid = array();

  for($i=0; $i<count($spw); $i++)
  {
    $stmt = $db->prepare("SELECT `id` FROM `2application` WHERE superpwr = :spw");
    $stmt ->execute(array(':spw' => $spw[$i]));
    $spwid[$i] = $stmt->fetchColumn();
  }
  
  for($i=0; $i<count($spw); $i++)
  {
    $stmt = $db->prepare("INSERT INTO `3application` VALUES (:userid, :spwid)");
    $stmt ->execute(array(':userid' => $userid[0]["id"], ':spwid' => $spwid[$i]));
  }

  header("Location: ?save=1");
}

catch(PDOException $e)
{
  print('Error : ' . $e->getMessage());
  exit();
}

?>
